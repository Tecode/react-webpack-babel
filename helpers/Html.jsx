import React from 'react';

const Html = ({ htmlContent, reqUrlObj, ...store }) => {
  const assets = webpackIsomorphicTools.assets();
  console.log(assets, '----------44');
  return (
    <html lang="zh">
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <title>react-ssr</title>
        {/* Styles will be presented in production with webpack extract text plugin */}
        {_.keys(assets.styles).map(style => (
          <link
            key={_.uniqueId()}
            href={assets.styles[style]}
            media="screen, projection"
            rel="stylesheet"
            type="text/css"
          />
        ))}
      </head>
      <body>
        <div
          id="react-view"
          style={{ height: '100%' }}
          // Rendering the route, which passed from server-side
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: htmlContent || '' }}
        />

        <script
          // Store the initial state into window
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html:
              store && `window.__INITIAL_STATE__=${JSON.stringify({a: 4})};`
          }}
        />
        {/* <script src="/vendors/js/polyfill.js" /> */}
        <script key={_.uniqueId()} src={assets.javascript.manifest} />
        <script key={_.uniqueId()} src={assets.javascript.vendor} />
        <script id="mainJs" key={_.uniqueId()} src={assets.javascript.main} />
      </body>
    </html>
  );
};

export default Html;
