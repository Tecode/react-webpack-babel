import React from 'react'
// import ReactDOM from 'react-dom'
import {AppContainer} from 'react-hot-loader'
import Routes from './routes'
import Loadable from 'react-loadable';
import { hydrate } from 'react-dom';

const renderApp = (Component) => {
  Loadable.preloadReady().then(() => {
    hydrate(
      <AppContainer>
        <Component/>
      </AppContainer>,
      document.getElementById('app')
    );
  })
};

renderApp(Routes);

// Webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./routes', () => {
    renderApp(require('./routes').default);
  })
}
