var webpack = require('webpack');
var path = require('path');
var loaders = require('./webpack.loaders');
// var HtmlWebpackPlugin = require('html-webpack-plugin');
var WebpackCleanupPlugin = require('webpack-cleanup-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

loaders.push({
  test: /\.scss$/,
  loader: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader?sourceMap&localIdentName=[local]___[hash:base64:5]!sass-loader?outputStyle=expanded'}),
  exclude: ['node_modules']
});

module.exports = {
  entry: [
    './src/server.js'
  ],
  output: {
    publicPath: './',
    path: path.join(process.cwd(), './public/assets'),
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash:8].chunk.js',
    pathinfo: true
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      "styles": path.resolve(__dirname, 'styles/'),
    }
  },
  module: {
    loaders
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      // filename: "vendor.js"
      // (给 chunk 一个不同的名字)
  
      minChunks: Infinity,
      // (随着 entry chunk 越来越多，
      // 这个配置保证没其它的模块会打包进 vendor chunk)
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest'
    }),
    new WebpackCleanupPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        drop_console: true,
        drop_debugger: true
      }
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new ExtractTextPlugin({
      filename: '[name].[contenthash:8].css',
      allChunks: true,
      disable: false, // Disable css extracting on development
      ignoreOrder: true
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    // new HtmlWebpackPlugin({
    //   template: './src/template.html',
    //   files: {
    //     css: ['style.css'],
    //     js: ['bundle.js'],
    //   }
    // })
  ]
};
